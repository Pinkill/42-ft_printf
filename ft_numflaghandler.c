/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_numflaghandler.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 13:30:40 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/02 13:33:25 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_numflaghandler(t_data *data, int size)
{
	int	i;

	i = 0;
	i += ft_zero(data, size);
	if (data->space_num > 0)
	{
		i += ft_fill((data->space_num - (size * (data->dot_num
		<= size)) - (data->dot_num * (data->dot_num > size))
		- (data->minus == 1)), ' ');
	}
	if ((data->minus == 1 && data->dot_num > size
		&& data->zero_num < data->dot_num)
		|| (data->minus == 1 && data->dot_num <= size
		&& data->zero_num <= size))
		i += ft_putchar('-');
	if (data->dot_num > size)
	{
		i += ft_fill((data->dot_num - size), '0');
	}
	return (i);
}
