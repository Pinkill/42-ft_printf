/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 18:02:28 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/02 13:31:14 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_H
# define LIBFTPRINTF_H

# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>

typedef struct	s_data
{
	char			type;
	int				dash_num;
	int				dot;
	int				dot_num;
	int				zero_num;
	int				space_num;
	int				minus;
}				t_data;

int				ft_atoi(const char **str);
int				ft_conversionhandle(t_data *data, long val);
int				ft_dash(t_data *data, int size, int num);
void			ft_defaultdata(t_data *data);
int				ft_fill(int n, char c);
int				ft_intsize(unsigned int n, int dev);
int				ft_longsize(unsigned long n, int dev);
int				ft_numflaghandler(t_data *data, int size);
int				ft_numhandler(t_data *data, va_list args, int *size);
int				ft_isconversion(const char **format, t_data *data);
void			ft_isflag(const char **format, t_data *data, va_list args);
int				ft_printf(const char *format, ...);
int				ft_putchar(char c);
int				ft_putnbr_base(unsigned int n, char *base);
int				ft_putnbr(int n);
int				ft_putnbrp_base(unsigned long n, char *base);
int				ft_putstr(const char **s, int n, int format);
int				ft_stringhandler(t_data *data, va_list args);
int				ft_strlen(const char *str);
int				ft_uputnbr(unsigned int n);
int				ft_zero(t_data *data, int size);

#endif
