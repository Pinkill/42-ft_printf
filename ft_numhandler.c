/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_numhandler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 13:25:54 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/02 13:26:12 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static int	ft_numhandler2(t_data *data, va_list args, int *size)
{
	int	val;

	if (data->type == 'u')
	{
		val = (unsigned int)va_arg(args, int);
		*size = (val == 0 && data->dot == 1 && data->dot_num == 0)
			? 0 : ft_intsize(val, 10);
	}
	else
	{
		val = va_arg(args, int);
		if (val < 0)
		{
			data->minus = 1;
			*size = ft_intsize(val * -1, 10);
		}
		else if (val == 0 && data->dot == 1 && data->dot_num == 0)
			*size = 0;
		else
			*size = ft_intsize(val, 10);
	}
	return (val);
}

int			ft_numhandler(t_data *data, va_list args, int *size)
{
	long	val;
	int		i;

	i = 0;
	val = 0;
	if (data->type == 'p')
	{
		val = (unsigned long)va_arg(args, void *);
		*size = ft_longsize(val, 16) + 2;
	}
	else if (data->type == 'x' || data->type == 'X')
	{
		val = va_arg(args, int);
		*size = (val == 0 && data->dot == 1 && data->dot_num == 0)
			? 0 : ft_intsize(val, 16);
	}
	else
		val = ft_numhandler2(data, args, size);
	i += ft_numflaghandler(data, *size);
	if (*size != 0)
		i += ft_conversionhandle(data, val);
	i += ft_dash(data, *size, 1);
	return (i);
}
