# 42 ft_printf function

A project in the 42 study program. This is another re-coding of a function from stdio.h, this time printf.

The end result is a static library libftprintf.a.

To see the documentation for the function and how it works, check "en.subject.pdf" file or "man 3 printf".

## Getting Started

This version of ft_printf(const char *format, ...) can do the following things:

```
- conversions (%c - character, %s - string, %p - pointer, %d and %i - int, %u - unsigned int, %x - hex, %X - hex big letters, %% - %)
- flags ('-' - right sided padding, 0 - zero padding, . - value precision, * - argument)
```

### Prerequisites

For linux and mac:

```
- gcc
- ar
- ranlib
```

### Installing

Getting the project:

```
git clone https://gitlab.com/Pinkill/42-ft_printf ftprintf
```

#### Building the static library

```
make all
```

#### Cleaning, rebuilding the project:

Removes library, but not the objects

```
make clean
```

Cleans whole project

```
make fclean
```

Cleans and rebuilds whole project

```
make re
```

Good luck!