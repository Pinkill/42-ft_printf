/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conversionhandle.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 20:01:06 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/30 19:45:28 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_conversionhandle(t_data *data, long val)
{
	int	i;

	i = 0;
	if (data->type == 'd' || data->type == 'i')
		i += ft_putnbr(val);
	else if (data->type == 'u')
		i += ft_uputnbr(val);
	else if (data->type == 'p')
		i += ft_putnbrp_base(val, "0123456789abcdef");
	else if (data->type == 'x')
		i += ft_putnbr_base(val, "0123456789abcdef");
	else if (data->type == 'X')
		i += ft_putnbr_base(val, "0123456789ABCDEF");
	return (i);
}
