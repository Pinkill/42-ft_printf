/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_zero.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 13:28:39 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/02 13:33:29 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_zero(t_data *data, int size)
{
	int	i;

	i = 0;
	if (data->zero_num > size && data->dot == 0)
	{
		if (data->minus == 1)
			i += ft_putchar('-');
		i += ft_fill((data->zero_num - size - (data->minus == 1)), '0');
	}
	else if (data->zero_num > size && data->dot_num >= 0 && data->dot == 1)
	{
		data->space_num = data->zero_num;
		data->zero_num = 0;
	}
	return (i);
}
