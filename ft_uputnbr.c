/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uputnbr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 21:03:02 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/26 21:06:15 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		ft_print_nb(unsigned int n, int *i)
{
	int	c;

	if (n > 0)
	{
		ft_print_nb(n / 10, i);
		c = (n % 10) + 48;
		*i += ft_putchar(c);
	}
}

int				ft_uputnbr(unsigned int n)
{
	int	i;

	i = 0;
	if (n == 0)
		return (ft_putchar('0'));
	ft_print_nb(n, &i);
	return (i);
}
