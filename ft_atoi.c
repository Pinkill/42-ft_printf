/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 19:14:42 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/26 19:14:53 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int		ft_atoi(const char **str)
{
	unsigned int		result;
	int					minus;

	result = 0;
	minus = 1;
	while (**str == '\t' || **str == '\n' || **str == '\v' || **str == '\f'
			|| **str == '\r' || **str == ' ')
		*str += 1;
	if (**str == '-' || **str == '+')
	{
		if (**str == '-')
			minus = -1;
		*str += 1;
	}
	while (**str != '\0' && **str >= '0' && **str <= '9')
	{
		result *= 10;
		result += (**str - '0');
		*str += 1;
	}
	return (result * minus);
}
