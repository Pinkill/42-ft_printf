/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 18:00:23 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/01 20:01:48 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static int	ft_find_conv(const char **format, t_data *data, va_list args)
{
	int	i;

	i = 0;
	if (**format == '%')
	{
		*format += 1;
		while (**format != '\0' && ft_isconversion(format, data) == 0)
			ft_isflag(format, data, args);
	}
	else if (**format != '%')
		i = ft_putstr(format, -1, 1);
	return (i);
}

int			ft_printf(const char *format, ...)
{
	va_list	arguments;
	t_data	data;
	int		ret;

	ret = 0;
	data.zero_num = 0;
	data.dot_num = 0;
	data.dot = 0;
	data.dash_num = 0;
	data.space_num = 0;
	data.type = 0;
	data.minus = 0;
	va_start(arguments, format);
	while (*format != '\0')
	{
		ret += ft_find_conv(&format, &data, arguments);
		if (data.type != 0)
			ret += ft_stringhandler(&data, arguments);
		ft_defaultdata(&data);
	}
	va_end(arguments);
	return (ret);
}
