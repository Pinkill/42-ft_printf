/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 18:34:31 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/01 17:48:37 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_putstr(const char **s, int n, int format)
{
	int	i;

	i = 0;
	while (**s != '\0' && n != 0)
	{
		if (**s == '%' && format == 1)
			break ;
		i += ft_putchar(**s);
		*s += 1;
		n -= (n != -1);
	}
	return (i);
}
