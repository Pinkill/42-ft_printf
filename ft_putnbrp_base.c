/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbrp_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 21:33:35 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/30 19:45:03 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		ft_print_nb(unsigned long n, char *base, int *i)
{
	if (n > 0)
	{
		ft_print_nb(n / 16, base, i);
		*i += ft_putchar(base[(n % 16)]);
	}
}

int				ft_putnbrp_base(unsigned long n, char *base)
{
	int	i;

	i = 2;
	ft_putchar('0');
	ft_putchar('x');
	if (n == 0)
		return (ft_putchar('0') + i);
	else
		ft_print_nb(n, base, &i);
	return (i);
}
