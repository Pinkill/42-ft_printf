/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isflag.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 18:09:16 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/01 17:04:41 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int			ft_isdigit(char c)
{
	if (c == '1' || c == '2' || c == '3' || c == '4' || c == '5'
		|| c == '6' || c == '7' || c == '8' || c == '9')
		return (1);
	return (0);
}

static void	ft_isflag_zerodash(const char **format, t_data *data, va_list args)
{
	if (**format == '-')
	{
		*format += 1;
		if (**format == '*')
		{
			data->dash_num = va_arg(args, int);
			data->dash_num *= (data->dash_num < 0) ? -1 : 1;
			*format += 1;
		}
		else if (**format != '-')
			data->dash_num = ft_atoi(format);
	}
	else if (**format == '0')
	{
		*format += 1;
		if (**format == '*')
		{
			data->zero_num = va_arg(args, int);
			data->dash_num = (data->zero_num * -1 * (data->zero_num < 0));
			data->zero_num *= (data->zero_num < 0) ? 0 : 1;
			*format += 1;
		}
		else if (**format != '0')
			data->zero_num = ft_atoi(format);
	}
}

static void	ft_isflag_dot(const char **format, t_data *data, va_list args)
{
	if (**format == '.')
	{
		*format += 1;
		data->dot = 1;
		if (**format == '*')
		{
			data->dot_num = va_arg(args, int);
			if (data->dot_num < 0)
			{
				data->dot = 0;
				data->dot_num = 0;
			}
			*format += 1;
		}
		else
			data->dot_num = ft_atoi(format);
	}
}

void		ft_isflag(const char **format, t_data *data, va_list args)
{
	ft_isflag_zerodash(format, data, args);
	if (ft_isdigit(**format))
		data->space_num = ft_atoi(format);
	else if (**format == '*')
	{
		data->space_num = va_arg(args, int);
		if (data->space_num < 0)
		{
			data->dash_num = data->space_num * -1;
			data->space_num = 0;
		}
		*format += 1;
	}
	ft_isflag_dot(format, data, args);
}
