/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stringhandler.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 18:51:10 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/02 13:33:27 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static int	ft_procflaghandler(t_data *data)
{
	int i;

	i = 0;
	if (data->zero_num > 1)
		i += ft_fill((data->zero_num - 1 - (data->minus == 1)), '0');
	if (data->space_num > 0)
		i += ft_fill((data->space_num - (1 * (data->dot_num
		>= 1)) - (data->dot_num * (data->dot_num < 1))
		- (1 * (data->dot_num == 0))), ' ');
	return (i);
}

static int	ft_strflaghandler(t_data *data, int size)
{
	int	i;

	i = 0;
	if (data->space_num > 0)
		i += ft_fill((data->space_num - (size * (data->dot_num
		>= size)) - (data->dot_num * (data->dot_num < size))
		- (size * (data->dot_num == 0))), ' ');
	return (i);
}

static int	ft_charhandler(t_data *data, va_list args)
{
	char	*val;
	int		i;
	int		size;

	i = 0;
	val = va_arg(args, char *);
	if (!val)
		val = "(null)";
	size = (data->dot == 1 && data->dot_num == 0) ? 0 : ft_strlen(val);
	i += ft_strflaghandler(data, size);
	i += (data->dot_num < size && data->dot == 1) ?
		ft_putstr((const char **)&val, data->dot_num, 0) :
		ft_putstr((const char **)&val, size, 0);
	i += ft_dash(data, size, 0);
	return (i);
}

static int	ft_charhandler2(t_data *data, va_list args)
{
	char	val;
	int		i;

	i = 0;
	if (data->type == 'c')
	{
		val = va_arg(args, int);
		i += ft_strflaghandler(data, 1);
	}
	else
		i += ft_procflaghandler(data);
	i += (data->type == 'c') ?
		ft_putchar(val) : ft_putchar('%');
	i += ft_dash(data, 1, 0);
	return (i);
}

int			ft_stringhandler(t_data *data, va_list args)
{
	int		i;
	int		size;

	i = 0;
	size = 0;
	if (data->type == 's' || data->type == 'c' || data->type == '%')
		i += (data->type == 's') ? ft_charhandler(data, args)
			: ft_charhandler2(data, args);
	else
		i += ft_numhandler(data, args, &size);
	return (i);
}
