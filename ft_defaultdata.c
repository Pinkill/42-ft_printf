/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_defaultdata.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 18:11:45 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/01 12:51:16 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	ft_defaultdata(t_data *data)
{
	data->zero_num = 0;
	data->dot_num = 0;
	data->dot = 0;
	data->dash_num = 0;
	data->space_num = 0;
	data->type = 0;
	data->minus = 0;
}
