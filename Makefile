# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/15 19:39:40 by mknezevi          #+#    #+#              #
#    Updated: 2019/12/02 13:33:07 by mknezevi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC = ft_atoi.c ft_defaultdata.c ft_isconversion.c ft_printf.c ft_putchar.c \
ft_putstr.c ft_isflag.c ft_putnbr.c ft_putnbr_base.c ft_putnbrp_base.c \
ft_uputnbr.c ft_conversionhandle.c ft_intsize.c ft_stringhandler.c ft_strlen.c \
ft_longsize.c ft_numflaghandler.c ft_numhandler.c ft_dash.c ft_zero.c ft_fill.c

BONUS = 

SRCS = ${addprefix ${PRE}, ${SRC}}

BONUSES = ${addprefix ${PRE}, ${BONUS}}

OBJS = ${SRCS:.c=.o}

BONUS_OBJS = ${BONUSES:.c=.o}

PRE = ./

HEAD = ./

NAME = libftprintf.a

AR = ar rc

RAN = ranlib

GCC = gcc

CFLAG = -Wall -Wextra -Werror

all:	$(NAME)

%.o: %.c
	${GCC} ${CFLAG} -c -I ${HEAD} $< -o ${<:.c=.o}

$(NAME): ${OBJS}
	${AR} ${NAME} ${OBJS}
	${RAN} ${NAME}

bonus:	${OBJS} ${BONUS_OBJS}
	${AR} ${NAME} ${BONUS} ${OBJS}
	${RAN} ${NAME}

clean:
	rm -f ${OBJS}
	rm -f ${BONUS_OBJS}

fclean:	clean
	rm -f ${NAME}

re: 	fclean all

.PHONY:		all clean fclean re