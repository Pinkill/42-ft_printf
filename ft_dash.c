/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dash.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 13:27:46 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/02 13:28:01 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_dash(t_data *data, int size, int num)
{
	int	i;

	i = 0;
	if (num == 0)
	{
		if (data->dash_num > 0)
			i += ft_fill((data->dash_num - (size * (data->dot_num >= size))
				- (data->dot_num * (data->dot_num < size)))
				- (size * (data->dot_num == 0 && data->dot == 0)), ' ');
	}
	else
	{
		size += (data->minus == 1);
		data->dot_num += (data->minus == 1);
		if (data->dash_num > 0)
			i += ft_fill((data->dash_num - (size * (data->dot_num <= size))
				- (data->dot_num * (data->dot_num > size))), ' ');
	}
	return (i);
}
