/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 21:27:41 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/26 21:34:07 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		ft_print_nb(unsigned int n, char *base, int *i)
{
	if (n > 0)
	{
		ft_print_nb(n / 16, base, i);
		*i += ft_putchar(base[(n % 16)]);
	}
}

int				ft_putnbr_base(unsigned int n, char *base)
{
	int	i;

	i = 0;
	if (n == 0)
		return (ft_putchar('0'));
	else
		ft_print_nb(n, base, &i);
	return (i);
}
