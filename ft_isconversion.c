/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isconversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 18:14:43 by mknezevi          #+#    #+#             */
/*   Updated: 2019/11/30 16:12:57 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_isconversion(const char **format, t_data *data)
{
	if (**format == 'c' || **format == 's' || **format == 'p' || **format == 'd'
		|| **format == 'i' || **format == 'u' || **format == 'x'
		|| **format == 'X' || **format == '%')
	{
		data->type = **format;
		*format += 1;
		return (1);
	}
	return (0);
}
